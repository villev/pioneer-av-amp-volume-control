﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PioneerVol
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int actualVol = 100;
        private NetworkStream str;
        TcpClient tcp = null;
        Timer readTimer = new Timer();
        StringBuilder rcv = new StringBuilder();

        public MainWindow()
        {
            InitializeComponent();
       }
        void GotVolume(int value)
        {
            actualVol = value;
            Vol.Value = value;
        }
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            var b = new byte[256];
            var len = str.Read(b, 0, 256);
            if (len > 0)
            {
                rcv.Append(Encoding.UTF8.GetString(b, 0, len));
                var parts = rcv.ToString().Replace("\r\n", "\n").Split('\n');
                if (parts.Length>1)
                {
                    for (int i = 0; i<parts.Length-1; i++) { ProcessLine(parts[i]); }
                    rcv.Clear();
                    rcv.Append(parts.Last());
                }
            }
        }

        private void ProcessLine(string v)
        {
            if (v.Length==6 && v.StartsWith("VOL"))
            {
                var vol = int.Parse(v.Substring(3));
                Vol.Dispatcher.Invoke(new Action(() => { GotVolume(vol/2); }));
            }
        }

        private void Vol_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int newVol = (int)Vol.Value;
            VolumeLabel.Content = newVol.ToString();
            var amo = actualVol - newVol;
            actualVol = newVol;
            if (amo!=0)
            {
                var dir = (byte)(amo<0 ? 'U' : 'D');
                if (amo<0) { amo = -amo; }

                int p = 0;
                var bu = new byte[amo * 3];
                {
                    bu[p++] = (byte)'V';
                    bu[p++] = dir;
                    bu[p++] = 13;
                }
                str.Write(bu, 0, bu.Length);
            }
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            readTimer.Enabled = false;
            tcp.Close();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            tcp = new TcpClient("192.168.1.56", 8102);
            tcp.NoDelay = true;
            str = tcp.GetStream();
            readTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            readTimer.Interval = 100;
            readTimer.Enabled = true;
            str.Write(Encoding.UTF8.GetBytes("?V\r"), 0, 3);
        }
    }
}
